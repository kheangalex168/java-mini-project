package helper;

import model.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

public class H {

    public static Product productPreview(Product product) {
        Table table = new Table(
                1,
                BorderStyle.UNICODE_BOX_HEAVY_BORDER,
                ShownBorders.SURROUND
        );
        table.setColumnWidth(0, 30, 30);
        table.addCell("ID             : " + product.getID());
        table.addCell("Name           : " + product.getName());
        table.addCell("Unit Price     : " + product.getUnitPrice());
        table.addCell("Qty            : " + product.getQty());
        table.addCell("Import Date    : " + product.getImportDate());
        System.out.println(table.render());
        return product;
    }

    public static double getDouble(String text, int min, double max) {
        double id;
        while (true) {
            try {
                System.out.print(text);
                Scanner scanner = new Scanner(System.in);
                id = scanner.nextDouble();
                if (id >= min && id <= max) return id;
                H.printWaveStyle(28);
                System.out.println("\tInput is negative number".toUpperCase());
                H.printWaveStyle(28);
            } catch (InputMismatchException e) {
                //reload input
            }

        }
    }

    public static String getString(String text) {
        Scanner scanner = new Scanner(System.in);
        System.out.print(text);
        return scanner.nextLine();
    }

    public static String getCurrentDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

    public static String getCurrentTimeAndDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

    public static int getInt(String text, int min, int max) {
        int id;
        while (true) {
            try {
                System.out.print(text);
                Scanner scanner = new Scanner(System.in);
                id = scanner.nextInt();
                if (id >= min && id <= max) {
                    return id;
                }
                H.printWaveStyle(28);
                System.out.println("\tInput is negative number".toUpperCase());
                H.printWaveStyle(28);
            } catch (InputMismatchException e) {
                //reload input
            }

        }
    }

    public static String getChoice() {
        System.out.print("Command --> ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    public static void asciiGenerator(int width, int height, String text, int millis) {
        //BufferedImage image = ImageIO.read(new File("/Users/mkyong/Desktop/logo.jpg"));
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();
        g.setFont(new Font("SansSerif", Font.PLAIN, 15));

        Graphics2D graphics = (Graphics2D) g;
        graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        graphics.drawString(text, 10, 20);

        //save this image
        //ImageIO.write(image, "png", new File("/users/mkyong/ascii-art.png"));

        for (int y = 0; y < height; y++) {
            StringBuilder sb = new StringBuilder();
            for (int x = 0; x < width; x++) {
                sb.append(image.getRGB(x, y) == -16777216 ? " " : "x");
            }

            if (sb.toString().trim().isEmpty()) {
                continue;
            }

            try{
                Thread.sleep(millis);
                System.out.println(sb);
            }catch (InterruptedException e){
                System.err.println(e.getMessage());
            }
        }
    }

    //    print wave style
    public static void printWaveStyle(int length) {
        System.out.print("○");
        for (int i = 0; i < length; i++) {
            System.out.print("~");
        }
        System.out.print("○\n");
    }

    //    print string with Tab number
    public static void printWithTab(int num, String text, boolean newLine) {
        for (int i = 0; i < num; i++) {
            System.out.print("\t");
        }
        if (newLine){
            System.out.print(text + "\n");
        }else{
            System.out.print(text);
        }
    }


    public static void printThread(String test, int millis) {
        for (int i = 0; i < test.length(); i++) {
            System.out.print(test.charAt(i));
            try {
                Thread.sleep(millis);    // time interval in milliseconds
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
